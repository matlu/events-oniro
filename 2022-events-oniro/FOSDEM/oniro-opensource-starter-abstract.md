# Oniro - an open-source starter for fast-paced IoT environments

## Track
Collaboration and Content Management devroom

https://fosdem.org/2022/schedule/track/collaboration_and_content_management/

## Title
Oniro - an open-source starter for fast-paced IoT environments

## Subtitle
A retrospective on an open-source project development

## Event type
Lecture (talk) 20 mins + 5 min panel

## Persons
Sebastian Serewa

Sebastian Serewa has 20-year experience of working in various domain projects. These range from Local Internet Registry administration, through DNS registrar transfer robot, to launching large scale new consumer electronics products in new markets.
Additionally, he gained software engineering hands-on-experience while working on soft realtime systems for various embedded devices.
Personally, a fan of software engineering, an author of a few articles in the area of operating systems development, their optimization and debugging. Recently he has been focused on enhancing his technical product management skills and searching for answers - how they could be applied to bring maximum value to the end users?

## Abstract

In this presentation Sebastian shares project experience gained over nearly 2 years of the Oniro project development in the area of products research & development. He starts with the initial assumptions and various criteria that team members intended to apply to the work done. It was a very interesting experience to apply a product-driven approach to highly-paced open-source projects nowadays. Thus, the maximum effort was put in order to receive maximum value from products intended to deliver to the market.

In this talk, Sebastian is going to present a few product management principles, how they were applied to the project and what lessons were learnt in this process. The talk is filled with a few open source projects (e.g. GitLab, OpenProject and others) and how they were applied to the tasks we were faced with. Work package structures and workflows applied were also described to supplement the presentation.

If you are building an open source community or are aiming to build one, this talk might be an interesting option for you.

## Links
Oniro: The Distributed Operating System That Connects Consumer Devices:
https://oniroproject.org/

Oniro sources:
https://booting.oniroproject.org/

Oniro Videos:
https://www.youtube.com/watch?v=p-gSvehb-As&list=PLy7t4z5SYNaQBDReZmeHAknEchYmu0LLa#OniroPlaylist

Our incubation and initial development environment:
https://git.ostc-eu.org/
