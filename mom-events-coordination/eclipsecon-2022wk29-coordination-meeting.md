## EclipseCon 2022 coordination meeting 2022wk29

Time: 
Participants: Aurore, Chiara, Donghi, Phil, Chiara, Amit, Adrian and Agustin

Community day
* We went over the community day.
* Philippe Coval named coordinator.
* Discussion about the format we would like to propose during the community day. We need to have information about the current plan from IoT&Edge guys.
* Phil will contact the coordinator for the community day on the IoT&Edge side.

Hackerspace
* Description of what the activity is about. Not all the details are known yet because the organising are shaping the activity.
* We need to define a coordinator.
* Phil to contact the coordinator from IoT&Edge to evaluate what it is planned.

Management
* Weekly calls until August instead of bi-weekly.
* Invite Frederic to one of them.

BoF room
* We will evaluate the need for a BoF.
